<?php

namespace App\DataFixtures\Document;

use App\DataFixtures\AppFixtures as DataFixturesAppFixtures;
use App\DataFixtures\ORM\AppFixtures;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use RuntimeException;
use Sulu\Bundle\DocumentManagerBundle\DataFixtures\DocumentFixtureInterface;
use Sulu\Bundle\MediaBundle\Entity\Media;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Bundle\SnippetBundle\Document\SnippetDocument;
use Sulu\Bundle\SnippetBundle\Snippet\DefaultSnippetManagerInterface;
use Sulu\Component\Content\Document\RedirectType;
use Sulu\Component\Content\Document\WorkflowStage;
use Sulu\Component\DocumentManager\DocumentManager;
use Sulu\Component\DocumentManager\Exception\DocumentManagerException;
use Sulu\Component\DocumentManager\Exception\MetadataNotFoundException;
use Sulu\Component\PHPCR\PathCleanup;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Parser;

class DocumentFixture implements DocumentFixtureInterface, ContainerAwareInterface
{
    private const WEBSITE_KEY = 'developersforfuture';

    use ContainerAwareTrait;
    /**
     * @var PathCleanup
     */
    private $pathCleanup;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /** @var DefaultSnippetManagerInterface */
    private $defaultSnippetManager;

    /**
     * @param SnippetDocument[] $writers
     */
    private $writers;

    public function getOrder(): int
    {
        return 10;
    }

    /**
     * @param mixed[] $data
     *
     * @throws MetadataNotFoundException
     */
    private function createSnippet(DocumentManager $documentManager, string $structureType, array $data): SnippetDocument
    {
        $locale = isset($data['locale']) && $data['locale'] ? $data['locale'] : AppFixtures::LOCALE_EN;

        /** @var SnippetDocument $snippetDocument */
        $snippetDocument = null;

        try {
            if (!isset($data['id']) || !$data['id']) {
                throw new Exception();
            }

            $snippetDocument = $documentManager->find($data['id'], $locale);
        } catch (Exception $e) {
            $snippetDocument = $documentManager->create('snippet');
        }

        $snippetDocument->getUuid();
        $snippetDocument->setLocale($locale);
        $snippetDocument->setTitle($data['title']);
        $snippetDocument->setStructureType($structureType);
        $snippetDocument->setWorkflowStage(WorkflowStage::PUBLISHED);
        $snippetDocument->getStructure()->bind($data);

        $documentManager->persist($snippetDocument, $locale, ['parent_path' => '/cmf/snippets']);
        $documentManager->publish($snippetDocument, $locale);

        return $snippetDocument;
    }

    /**
     * @throws DocumentManagerException
     */
    private function updatePages(DocumentManager $documentManager, string $locale): void
    {
        /** @var BasePageDocument $artistsDocument */
        $artistsDocument = $documentManager->find('/cmf/developersforfuture/contents', $locale);

        $data = $artistsDocument->getStructure()->toArray();

        $data['elements'] = [
            'sortBy' => 'published',
            'sortMethod' => 'asc',
            'dataSource' => $artistsDocument->getUuid(),
        ];

        $artistsDocument->getStructure()->bind($data);

        $documentManager->persist($artistsDocument, $locale);
        $documentManager->publish($artistsDocument, $locale);
    }

    /**
     * @param mixed[] $data
     *
     * @throws MetadataNotFoundException
     */
    private function createPage(DocumentManager $documentManager, array $data): BasePageDocument
    {
        $locale = isset($data['locale']) && $data['locale'] ? $data['locale'] : AppFixtures::LOCALE_EN;

        if (!isset($data['url'])) {
            $url = $this->getPathCleanup()->cleanup('/' . $data['title']);
            if (isset($data['parent_path'])) {
                $url = mb_substr($data['parent_path'], mb_strlen('/cmf/developersforfuture/contents')) . $url;
            }

            $data['url'] = $url;
        }

        $extensionData = [
            'seo' => $data['seo'] ?? [],
            'excerpt' => $data['excerpt'] ?? [],
        ];

        unset($data['excerpt']);
        unset($data['seo']);

        /** @var PageDocument $pageDocument */
        $pageDocument = null;

        try {
            if (!isset($data['id']) || !$data['id']) {
                throw new Exception();
            }

            $pageDocument = $documentManager->find($data['id'], $locale);
        } catch (Exception $e) {
            $pageDocument = $documentManager->create('page');
        }

        $pageDocument->setNavigationContexts($data['navigationContexts'] ?? []);
        $pageDocument->setLocale($locale);
        $pageDocument->setTitle($data['title']);
        $pageDocument->setResourceSegment($data['url']);
        $pageDocument->setStructureType($data['structureType'] ?? 'default');
        $pageDocument->setWorkflowStage(WorkflowStage::PUBLISHED);
        $pageDocument->getStructure()->bind($data);
        $pageDocument->setAuthor(1);
        $pageDocument->setExtensionsData($extensionData);

        if (isset($data['redirect'])) {
            $pageDocument->setRedirectType(RedirectType::EXTERNAL);
            $pageDocument->setRedirectExternal($data['redirect']);
        }

        $documentManager->persist(
            $pageDocument,
            $locale,
            ['parent_path' => $data['parent_path'] ?? '/cmf/developersforfuture/contents']
        );

        // Set dataSource to current page after persist as uuid is before not available
        if (isset($data['pages']['dataSource']) && '__CURRENT__' === $data['pages']['dataSource']) {
            $pageDocument->getStructure()->bind(
                [
                    'pages' => array_merge(
                        $data['pages'],
                        [
                            'dataSource' => $pageDocument->getUuid(),
                        ]
                    ),
                ]
            );

            $documentManager->persist(
                $pageDocument,
                $locale,
                ['parent_path' => $data['parent_path'] ?? '/cmf/developersforfuture/contents']
            );
        }

        $documentManager->publish($pageDocument, $locale);

        return $pageDocument;
    }

    private function getPathCleanup(): PathCleanup
    {
        if (null === $this->pathCleanup) {
            $this->pathCleanup = $this->container->get('sulu.content.path_cleaner');
        }

        return $this->pathCleanup;
    }

    private function getEntityManager(): EntityManagerInterface
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
        }

        return $this->entityManager;
    }

    private function getDefaultSnippetManager(): DefaultSnippetManagerInterface
    {
        if (null === $this->defaultSnippetManager) {
            $this->defaultSnippetManager = $this->container->get('sulu_snippet.default_snippet.manager');
        }

        return $this->defaultSnippetManager;
    }

    private function getMediaId(string $name): int
    {
        try {
            $id = $this->getEntityManager()->createQueryBuilder()
                ->from(Media::class, 'media')
                ->select('media.id')
                ->innerJoin('media.files', 'file')
                ->innerJoin('file.fileVersions', 'fileVersion')
                ->where('fileVersion.name = :name')
                ->setMaxResults(1)
                ->setParameter('name', $name)
                ->getQuery()->getSingleScalarResult();

            return (int) $id;
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException(sprintf('Too many images with the name "%s" found.', $name), 0, $e);
        }
    }

        /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return mixed[]
     * @throws MetadataNotFoundException
     *
     */
    private function loadPages(DocumentManager $documentManager): array
    {
        $yaml = new Parser();
        $configFilePath = __DIR__ . '/../../Resources/data/static_pages.yml';
        $content = file_get_contents($configFilePath);

        $pages = [];
        if ($content === false) {
            throw new \Exception("'$configFilePath' could not be read.");
        }

        $data = $yaml->parse($content);
        foreach ($data['static'] as $pageData) {
            $pageData['parent_path'] = '/cmf/developersforfuture/contents';
            $pages[$pageData['url']] = $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }

    private function loadArticles(DocumentManager $documentManager): array
    {
        $yaml = new Parser();
        $configFilePath = __DIR__ . '/../../Resources/data/articles.yml';
        $content = file_get_contents($configFilePath);

        $pages = [];
        if ($content === false) {
            throw new \Exception("'$configFilePath' could not be read.");
        }
        $pageData = [
            'locale' => AppFixtures::LOCALE_EN,
            'title' => 'Articles',
            'structureType' => 'article_selection',
            'url' => '/articles',
            'intro' => '<p>We collect our own articles and announcements for the organization itself and the environment.</p>',
            'parent_path' => '/cmf/developersforfuture/contents',
            'navigationContexts' =>  ['main'],
            'seo' => [
                'title' => 'Blogging, News and other Articles',
                'description' => '',
            ],
        ];

        $this->createPage($documentManager, $pageData);
        $data = $yaml->parse($content);
        foreach ($data['articles'] as $pageData) {
            $pageData['structureType'] = 'article';
            $pageData['local'] = AppFixtures::LOCALE_EN;
            $pageData['parent_path'] = '/cmf/developersforfuture/contents/articles';
            if (isset($pageData['blocks']) && count($pageData['blocks']) && is_array($pageData['blocks'])) {
                foreach ($pageData['blocks'] as $key => $value) {
                    if ($value['type'] === 'image_block' || $value['type'] === 'card_image_block') {
                        $pageData['blocks'][$key]['image'] = [ 'id' => $this->getMediaId($value['image'])];
                    }
                }
            }
            
            $pages[$pageData['name']] = $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }
    private function loadProjects(DocumentManager $documentManager): array
    {
        $yaml = new Parser();
        $configFilePath = __DIR__ . '/../../Resources/data/projects.yml';
        $content = file_get_contents($configFilePath);

        $pages = [];
        if ($content === false) {
            throw new \Exception("'$configFilePath' could not be read.");
        }
        $pageData = [
            'name' => 'projects',
            'locale' => AppFixtures::LOCALE_EN,
            'title' => 'Projects',
            'intro' => 'tbd.',
            'structureType' => 'project_selection',
            'url' => '/projects',
            'intro' => '<p>It turned out that we have now implemented many projects to achieve our goals. Here you get a selection of our projects. For this purpose, we also want to enter projects from others here that make their own contribution to climate protection.</p>',
            'parent_path' => '/cmf/developersforfuture/contents',
            'navigationContexts' =>  ['main'],
            'seo' => [
                'title' => 'Projecte we are working on',
                'description' => 'tbd.',
            ]
        ];

        $pages[$pageData['name']] = $this->createPage($documentManager, $pageData);
        $documentManager->flush();
        $data = $yaml->parse($content);
        foreach ($data['projects'] as $pageData) {
            $pageData['structureType'] = 'project';
            $pageData['local'] = AppFixtures::LOCALE_EN;
            $pageData['parent_path'] = '/cmf/developersforfuture/contents/projects';
            if (isset($pageData['blocks']) && count($pageData['blocks']) && is_array($pageData['blocks'])) {
                foreach ($pageData['blocks'] as $key => $value) {
                    if ($value['type'] === 'image_block' || $value['type'] === 'card_image_block') {
                        $pageData['blocks'][$key]['image'] = [ 'id' => $this->getMediaId($value['image'])];
                    }
                }
            }
            
            $pages[$pageData['name']] = $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }

    private function loadPublications(DocumentManager $documentManager): array
    {
        $yaml = new Parser();
        $configFilePath = __DIR__ . '/../../Resources/data/publications.yml';
        $content = file_get_contents($configFilePath);

        $pages = [];
        if ($content === false) {
            throw new \Exception("'$configFilePath' could not be read.");
        }

        $data = $yaml->parse($content);

        foreach ($data['publications'] as $pageData) {
            $pageData['structureType'] = 'publication';
            $pages[$pageData['name']] = $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }

    private function loadPress(DocumentManager $documentManager): array
    {
        $yaml = new Parser();
        $configFilePath = __DIR__ . '/../../Resources/data/press.yml';
        $content = file_get_contents($configFilePath);

        $pages = [];
        if ($content === false) {
            throw new \Exception("'$configFilePath' could not be read.");
        }

        $data = $yaml->parse($content);

        foreach ($data['press'] as $pageData) {
            $pageData['structureType'] = 'press';
            if (isset($pageData['blocks']) && count($pageData['blocks']) && is_array($pageData['blocks'])) {
                foreach ($pageData['blocks'] as $key => $value) {
                    if ($value['type'] === 'download') {
                        $pageData['blocks'][$key]['download'] = [ 'id' => $this->getMediaId($value['download'])];
                    }
                }
            }
            
            $pages[$pageData['name']] = $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }

    /**
     * @throws Exception
     */
    private function loadWriterSnippet(DocumentManager $documentManager): void
    {
        $data = [
            'locale' => AppFixtures::LOCALE_EN,
            'title' => 'Maximilian Berghoff',
            'description' => '<p>I work as a developer for the Mayflower GmbH in Würzburg. As such, I develop highly professional PHP and javascript applications. I invest my free time in contributing to the Symfony CMF (Content Management Framework). Besides I am a dad of two lovely sons and enjoy reading and watching Game of Thrones.</p>',
            'web_url' => 'https://maximilian-berghoff.de',
            'twitter_url' => 'https://twitter.com/EelctricMaxxx',
            'facebook_url' => 'https://facebook.com/maximilian.berghoff',
            'github_url' => 'https://github.com/electricmaxxx',
            'gitlab_url' => 'https://gitlab.com/electricmaxxx',
        ];

        $snippetDocument = $this->createSnippet($documentManager, 'contributor', $data);

        $this->getDefaultSnippetManager()->save('developersforfuture', 'contributor', $snippetDocument->getUuid(), AppFixtures::LOCALE_EN);
    }

    /**
     * @throws DocumentManagerException
     */
    private function loadHomepage(DocumentManager $documentManager): void
    {
        /** @var HomeDocument $homeDocument */
        $homeDocument = $documentManager->find('/cmf/developersforfuture/contents', AppFixtures::LOCALE_EN);

        $homeDocument->getStructure()->bind(
            [
                'locale' => AppFixtures::LOCALE_EN,
                'title' => $homeDocument->getTitle(),
                'structureType' => 'homepage',
                'header_image' => $this->getMediaId('KlimastreiNov_FB_post_1200x630 copy.jpg'),
                'parent_path' => '/cmf/developersforfuture/contents',
                'url' => '/',
                'seo' => [
                    'title' => 'Developers For Future',
                    'description' => '',
                ],
                'pillars' => [
                    [
                        'type' => 'pillar',
                        'title' => 'Support the movement',
                        'text' => '<p>Every organisation within the climate movement has its own site on the web, different requirements for existing systems or simply needs an IT specialist to present its content in an orderly and secure way. This is where we come in and have become a permanent contact for everyone who feels part of the FridaysForFuture movement!</p>'
                    ],
                    [
                        'type' => 'pillar',
                        'title' => 'Education',
                        'text' => '<p>Working together on web developments creates its own dynamic in terms of training and further education - the classic learning by doing and communication with each other can help everyone to make progress in their own further education phase.</p><p>However, there is also a great demand for digital further education opportunities for kids in schools, as there are too few specialists available. Digitalisation is the future of our children and dealing with it should be realised for you. </p>',
                    ],
                    [
                        'type' => 'pillar',
                        'title' => 'Explore',
                        'text' => '<p>In IT, we live in a field of tension between drivers and finding solutions to the climate crisis. We can consume as much electricity with server farms as entire countries, but we can offer solution approaches for example with smart grids for the power supply. We want to investigate this area of tension together with our friendly friends and work out solutions.</p>',
                    ],
                    [
                        'type' => 'pillar',
                        'title' => 'Projects',
                        'text' => '<p>Within the support of the ForFuture organizations, great ideas and solutions are always crystallized in discussions with other developers and contributors. The DevelopersForFuture start here and turn ideas into real projects that should be implemented in the form of applications.</p><p>We present some of the previously implemented pages listed and update them regularly. In order to be able to counter the climate crisis, to educate people or simply to point out the problems, further projects should be launched and implemented.</p><p>Since this results in expenses that can no longer only be covered in terms of leisure time, we must also point out the perspective of financial support. The basic idea here is transparent. Therefore we ask for your support via Open Collective.</p>',
                        'url' => '/#projects',
                    ],
                ],
                'articles_title' => 'Articles',
                'articles_text' => '<p>We collect our own articles and announcements for the organization itself and the environment.</p>',
                'projects_title' => 'Projects',
                'projects_text' => '<p>It turned out that we have now implemented many projects to achieve our goals. Here you get a selection of our projects. For this purpose, we also want to enter projects from others here that make their own contribution to climate protection.</p>',
            ]
        );

        $documentManager->persist($homeDocument, AppFixtures::LOCALE_EN);
    }

    /**
     * @throws Exception
     */
    private function loadEventSnippets(DocumentManager $documentManager): array
    {
        $yaml = new Parser();
        $configFilePath = __DIR__ . '/../../Resources/data/events.yml';
        $content = file_get_contents($configFilePath);

        $events = [];
        if ($content === false) {
            throw new \Exception("'$configFilePath' could not be read.");
        }

        $data = $yaml->parse($content);
        foreach ($data['events'] as $docData) {
            $snippetDocument = $this->createSnippet($documentManager, 'events', $docData);

            $this->getDefaultSnippetManager()->save('developersforfuture', 'events', $snippetDocument->getUuid(), AppFixtures::LOCALE_EN);
            $events[$docData['title']] = $snippetDocument;
        }

        return $events;
    }

      /**
     * @throws DocumentManagerException
     * @throws MetadataNotFoundException
     * @throws Exception
     */
    public function load(DocumentManager $documentManager): void
    {
        $this->loadPages($documentManager);
        #$this->loadContactSnippet($documentManager);
        $this->loadHomepage($documentManager);
        $this->loadEventSnippets($documentManager);
        $this->loadWriterSnippet($documentManager);
        $this->loadArticles($documentManager);
        $this->loadProjects($documentManager);
        $this->loadPublications($documentManager);
        $this->loadPress($documentManager);

        // Needed, so that a Document use by loadHomepageGerman is managed.
        $documentManager->flush();
    }
}
